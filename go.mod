module main

go 1.16

require (
	github.com/appleboy/gin-jwt/v2 v2.6.4 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
)
