package main
///signature server prototype
import (
	//	"fmt"
	"log"
	"net/http"
	"os"
	"github.com/google/uuid"
	//"math/rand"
	"time"
	"crypto/rand"
	"crypto/rsa"
	//	"crypto/sha256"
	//	"encoding/base64"
	"github.com/gin-gonic/gin"
	"github.com/appleboy/gin-jwt/v2"
	"io/ioutil"
	"encoding/json"
)

//var mySigningKey = []byte("captainjacksparrowsayshi")

type UserAccount struct {
	UserID string `json:"UserID"`
	ApiKey string `json:"apiKey" binding:"required"`
	ApiSecret string `json:"apiSecret" binding:"required"`
}

type SigningDevice struct {
	UUID string `json:"UUID"`
	Label string `json:"Label"`
	Transactions int `json:"transactions"`
}

type Transaction struct {
	TxId string `json:"txId"`
	State string `json:"state"`
	Dummydata string `json: "transactioncontents"`
}


//test user
var TestUsers []UserAccount
//signature Devices
var RegisteredSignatureDevices []SigningDevice
var DeviceKeys = make(map[string] *rsa.PrivateKey)
var Transactions []Transaction

func handlerFunctions() {
	port := os.Getenv("PORT")
	//router := gin.New()
	router := gin.Default()
	//router.Use(gin.Logger())
	//router.Use(gin.Recovery())
	if port == "" {
		port = "10000"
	}
	var identityKey = "id"
	//setting up JWT as Middleware
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Key: []byte("secretsigningkey"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		/*	IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &UserAccount{
				UserName: claims[identityKey].(string),
			}
		},*/
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var newUser UserAccount
			if err := c.ShouldBind(&newUser); err != nil {
				//if err := c.BindJSON(&newUser); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			
			userkey := newUser.ApiKey
			password := newUser.ApiSecret
			for _,x:= range TestUsers{				
				if (userkey == x.ApiKey && password == x.ApiSecret) {
					return &UserAccount{
						UserID: x.UserID,
						ApiKey: userkey,
						//ApiSecret: password,
					}, nil
				}
			}
			return nil, jwt.ErrFailedAuthentication
		},
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if _, ok := data.(*UserAccount); ok {
				return jwt.MapClaims{
					identityKey: data.(*UserAccount).UserID,
				}
			}
			return jwt.MapClaims{}
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": "unauthorized",
			})
		},
		//TokenLookup: "header: Authorization, query: token",
	})
	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	///Endpoints:	
	router.POST("/auth", authMiddleware.LoginHandler)
	router.Use(authMiddleware.MiddlewareFunc())
	{
		router.PUT("/newDevice", creatNewDevice)
		router.GET("/Device/:id", fetchDevice)
		router.DELETE("/Device/:id", deleteDevice)
		router.GET("/Device", listAllDevices)
		router.PUT("/Device/:id/tx/", startTransaction)
		router.PUT("/Device/:id/tx/:txid", updateTransaction)
	}
	if err := http.ListenAndServe(":"+port, router); err != nil {
		log.Fatal(err)
	}
	//router.Run("localhost:10000")

}

func creatNewDevice(c *gin.Context){
	//claims := jwt.ExtractClaims(c)
	var newSigningDevice SigningDevice
	uuidForDevice := uuid.New()
	newSigningDevice.UUID = uuidForDevice.String()
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil{
		return
	}
	//newSigningDevice.Key=privateKey
	newSigningDevice.Label="temp"
	newSigningDevice.Transactions=0
	DeviceKeys[newSigningDevice.UUID] = privateKey
	c.IndentedJSON(http.StatusOK, newSigningDevice)
	RegisteredSignatureDevices = append(RegisteredSignatureDevices, newSigningDevice)
	dat, err := json.Marshal(RegisteredSignatureDevices)
	if err!= nil {
		return
	}
	err = ioutil.WriteFile("./data/Devices.JSON", dat, 0644)
	if err!= nil {
		return
	}
}


func fetchDevice(c *gin.Context){
	
	id := c.Param("id")
	for _, a := range RegisteredSignatureDevices {
		if a.UUID == id {
			c.IndentedJSON(http.StatusOK, a)
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Device not found"})
}

func deleteDevice(c *gin.Context){
	
	id := c.Param("id")
	for i, a := range RegisteredSignatureDevices {
		if a.UUID == id {
			c.IndentedJSON(http.StatusOK, a)
			RegisteredSignatureDevices = append(RegisteredSignatureDevices[:i], RegisteredSignatureDevices[i+1:]...)
			delete(DeviceKeys, id)
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Device not found"})
}

func listAllDevices(c *gin.Context){
	c.IndentedJSON(http.StatusOK, RegisteredSignatureDevices)
	return
}

func startTransaction(c *gin.Context){
	var newTransaction Transaction
	uuidForTrans := uuid.New()
	newTransaction.TxId = uuidForTrans.String()
	newTransaction.State = "Active"
	c.IndentedJSON(http.StatusOK, newTransaction)
	Transactions = append(Transactions, newTransaction)
	return
}

func updateTransaction(c *gin.Context){
	id := c.Param("txid")
	var newTransaction Transaction
	if err := c.ShouldBind(&newTransaction); err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Device not found"})
		for _, a := range Transactions {
			if a.TxId == id {
				a.Dummydata=newTransaction.Dummydata
			}
			return
		}
		c.IndentedJSON(http.StatusOK, newTransaction)	
	}
}

func main() {
	//setting up an array of testusers
	TestUsers = []UserAccount{
	UserAccount {UserID: "Testuser1", ApiKey: "apiTestKey", ApiSecret: "apiTestSecret"},
	UserAccount {UserID: "Testuser2", ApiKey: "apiTestKey2", ApiSecret: "apiTestSecret2"},
	}

	handlerFunctions()
}
